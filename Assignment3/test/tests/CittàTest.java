package tests;
import static org.junit.Assert.*;
import helpers.PersistenceHelperSingleton;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dataset.Datas;
import entities.Aeroporto;
import entities.Città;
import services.CittàService;


public class CittàTest {
	private CittàService cs;
	private Datas data;
	
	@Before
	public void boot(){
		cs = new CittàService();
		data = new Datas();
	}
	
	@org.junit.Test
    public void get() {
        Città c = data.getCittà().get(0);
        cs.persist(c);
        c = cs.get(c.getId());
        assertNotNull(c.getNome());
    }
	

	
	
	@org.junit.Test
    public void update() {
		Città c = data.getCittà().get(1);
        cs.persist(c);
        cs.updateName(c.getId(), "Milano MI");
        Città u = cs.get(c.getId());
        assertEquals("Milano MI", u.getNome());

    }
	
	@org.junit.Test
    public void remove() {
        Città c = data.getCittà().get(2);
        cs.persist(c);
        cs.remove(c.getId());
        c = cs.get(c.getId());
        assertNull(c);
    }
	
	@org.junit.Test
    public void getAll() {
        Città c = data.getCittà().get(3);
        Città v = data.getCittà().get(4);
        cs.persist(c);
        cs.persist(v);
        assertEquals(2, cs.getAll().size());
        
      
    }
	
	@org.junit.Test
    public void getWhere() {
		
        Città c = data.getCittà().get(5);
        Città v = data.getCittà().get(6);
        cs.persist(c);
        cs.persist(v);
        assertEquals(1, cs.getWhere("nazione", "Germania").size());
        cs.remove(c.getId());
        cs.remove(v.getId());
    }
	
	@AfterClass
    public static void clearDb() {
		
        PersistenceHelperSingleton ph = PersistenceHelperSingleton.getInstance();
        ph.clearDatabase();
    }

	
	


}
