package tests;
import static org.junit.Assert.*;

import java.util.ArrayList;

import helpers.PersistenceHelperSingleton;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dataset.Datas;
import entities.Aeroporto;
import entities.Città;
import entities.Compagnia;
import entities.Volo;
import services.AeroportoService;
import services.CittàService;
import services.CompagniaService;
import services.VoloService;


public class VoloTest {

	private Datas data;
	
	private ArrayList<Città> citi;
	private ArrayList<Aeroporto> aeroporti;
	private ArrayList<Compagnia> compagnie;
	
	
	private VoloService vs;
	private CittàService cs;
	private AeroportoService as;
	private CompagniaService ccs;
	
	@Before
	public void boot(){
	
		vs = new VoloService();
		data = new Datas();
		cs = new CittàService();
		as = new AeroportoService();
		ccs = new CompagniaService();
		citi = data.getCittà();
		
		cs.persist(citi.get(0));
		cs.persist(citi.get(1));
		cs.persist(citi.get(2));
		cs.persist(citi.get(3));
		cs.persist(citi.get(4));
		cs.persist(citi.get(5));
		cs.persist(citi.get(6));
		cs.persist(citi.get(7));
		
		as.persist(data.getAeroporti().get(0));
		as.persist(data.getAeroporti().get(1));
		as.persist(data.getAeroporti().get(2));
		as.persist(data.getAeroporti().get(3));
		as.persist(data.getAeroporti().get(4));
		as.persist(data.getAeroporti().get(5));
		as.persist(data.getAeroporti().get(6));
		as.persist(data.getAeroporti().get(7));
		
		
		ccs.persist(data.getCompagnie().get(0));
		ccs.persist(data.getCompagnie().get(1));
		ccs.persist(data.getCompagnie().get(2));
		ccs.persist(data.getCompagnie().get(3));
		ccs.persist(data.getCompagnie().get(4));
		ccs.persist(data.getCompagnie().get(5));
		
		
		
		
		
	
		
	}
	
	@org.junit.Test
    public void get() {
        Volo c = data.getVoli().get(4);
        vs.persist(c);
        c = vs.get(c.getFlightcode());
        assertNotNull(c.getAereomobile());
    }
	
	@org.junit.Test
    public void getAll() {
        Volo c = data.getVoli().get(0);
        Volo v = data.getVoli().get(1);
        vs.persist(c);
        vs.persist(v);
        assertEquals(2, vs.getAll().size());
      
    }
	
	@org.junit.Test
    public void getWhere() {
		
        Volo c = data.getVoli().get(2);
        Volo v = data.getVoli().get(3);
        vs.persist(c);
        vs.persist(v);
        assertEquals(2, vs.getWhere("aereomobile", "BOEING 616-300").size());
        
    }
	
	@org.junit.Test
    public void updateAereo() {
		Volo c = data.getVoli().get(6);
        vs.persist(c);
        vs.updateAicraft(c.getFlightcode(), "Boing 747-891");
        Volo u = vs.get(c.getFlightcode());
        assertEquals("Boing 747-891", u.getAereomobile());
    }
	@org.junit.Test
    public void updateDestinazione() {
	
		Volo c = data.getVoli().get(5);
		String newDest = data.getAeroporti().get(4).getAirportCode();
        vs.persist(c);
        vs.updateDestinazione(c.getFlightcode(), newDest);
        Volo u = vs.get(c.getFlightcode());
        assertEquals(c.getArrivo(), u.getArrivo());
    }
	
	@org.junit.Test
    public void remove() {
		Volo p = data.getVoli().get(7);
        Volo c = data.getVoli().get(8);
        vs.persist(p);
        vs.persist(c);
        vs.remove(c.getFlightcode());
        c = vs.get(c.getFlightcode());
        assertNull(c);
    }
	
	@AfterClass
    public static void clearDb() {
        PersistenceHelperSingleton ph = PersistenceHelperSingleton.getInstance();
        ph.clearDatabase();
    }
	

	
	


}
