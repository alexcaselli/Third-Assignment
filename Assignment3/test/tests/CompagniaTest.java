package tests;
import static org.junit.Assert.*;
import helpers.PersistenceHelperSingleton;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dataset.Datas;
import entities.Aeroporto;
import entities.Compagnia;
import services.CompagniaService;



public class CompagniaTest {
	private CompagniaService cs;
	private Datas data;
	
	@Before
	public void boot(){
		cs = new CompagniaService();
		data = new Datas();
	}
	
	@org.junit.Test
    public void get() {
        Compagnia c = data.getCompagnie().get(0);
        cs.persist(c);
        c = cs.get(c.getCompanyCode());
        assertNotNull(c.getNome());
    }
	
	@org.junit.Test
    public void getAll() {
        Compagnia c = data.getCompagnie().get(1);
        Compagnia v = data.getCompagnie().get(2);
        cs.persist(c);
        cs.persist(v);
        assertEquals(2, cs.getAll().size());
    }
	
	@org.junit.Test
    public void getWhere() {
		
        Compagnia c = data.getCompagnie().get(3);
        Compagnia v = data.getCompagnie().get(4);
        cs.persist(c);
        cs.persist(v);
        assertEquals(1, cs.getWhere("nome", "NorwayAir").size());

    }
	
	@org.junit.Test
    public void update() {
		Compagnia c = data.getCompagnie().get(5);
        cs.persist(c);
        cs.updateName(c.getCompanyCode(), "AirItaly");
        Compagnia u = cs.get(c.getCompanyCode());
        assertEquals("AirItaly", u.getNome());
 
    }
	
	@org.junit.Test
    public void remove() {
        Compagnia c = data.getCompagnie().get(5);
        cs.persist(c);
        cs.remove(c.getCompanyCode());
        c = cs.get(c.getCompanyCode());
        assertNull(c);
    }
	
	@AfterClass
    public static void clearDb() {
        PersistenceHelperSingleton ph = PersistenceHelperSingleton.getInstance();
        ph.clearDatabase();
    }

	
	


}
