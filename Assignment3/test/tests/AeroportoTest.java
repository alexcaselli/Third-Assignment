package tests;
import static org.junit.Assert.*;

import java.util.ArrayList;

import helpers.PersistenceHelperSingleton;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dataset.Datas;
import entities.Aeroporto;
import entities.Città;
import services.AeroportoService;
import services.CittàService;


public class AeroportoTest {
	private AeroportoService as;
	private CittàService cs;
	private Datas data;
	ArrayList<Città> citi;
	
	@Before
	public void boot(){
		as = new AeroportoService();
		cs = new CittàService();
		
		data = new Datas();
		
		citi = data.getCittà();
		cs.persist(citi.get(0));
		cs.persist(citi.get(1));
		cs.persist(citi.get(2));
		cs.persist(citi.get(3));
		cs.persist(citi.get(4));
		cs.persist(citi.get(5));
		cs.persist(citi.get(6));
		cs.persist(citi.get(7));
	}
	
	@org.junit.Test
    public void get() {
        Aeroporto c = data.getAeroporti().get(0);
        as.persist(c);
        c = as.get(c.getAirportCode());
        assertNotNull(c.getNome());

    }
	
	@org.junit.Test
    public void getAll() {
		
        Aeroporto c = data.getAeroporti().get(1);
        Aeroporto v = data.getAeroporti().get(2);
        as.persist(c);
        as.persist(v);
        assertEquals(2, as.getAll().size());
    }
	
	@org.junit.Test
    public void getWhere() {
		
        Aeroporto c = data.getAeroporti().get(3);
        Aeroporto v = data.getAeroporti().get(4);
        as.persist(c);
        as.persist(v);
        assertEquals(1, as.getWhere("nome", "Aeroporto di Faro").size());
       
    }
	
	@org.junit.Test
    public void update() {
		Aeroporto c = data.getAeroporti().get(5);
		c.setCittà(citi.get(5));
        as.persist(c);
        
        as.updateName(c.getAirportCode(), "Aeroporto di Munchen");
        Aeroporto u = as.get(c.getAirportCode());
        assertEquals("Aeroporto di Munchen", u.getNome());
 
    }
	
	@org.junit.Test
    public void remove() {
        Aeroporto c = data.getAeroporti().get(6);
        c.setCittà(citi.get(0));
        as.persist(c);
        as.remove(c.getAirportCode());
        c = as.get(c.getAirportCode());
        assertNull(c);
     
    }
	
	
	@AfterClass
    public static void clearDb() {
		
        PersistenceHelperSingleton ph = PersistenceHelperSingleton.getInstance();
        ph.clearDatabase();
    }

	
	


}
