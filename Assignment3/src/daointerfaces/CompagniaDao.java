package daointerfaces;

import java.util.List;

import entities.Aeroporto;
import entities.Compagnia;

public interface CompagniaDao {
	public int persistCompagnia(Compagnia c);
	public Compagnia findCompagnia(String companyCode);
	public List<Compagnia> getAll();
	public void removeCompagnia(String companyCode);
	public void updateCompagnia(Compagnia c);
	public List<Compagnia> getWhere(String field, String value);


}
