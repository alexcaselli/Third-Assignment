package daointerfaces;

import java.util.List;

import entities.Aeroporto;
import entities.Città;

public interface CittàDao {
	public int persistCittà(Città c);
	public Città findCittà(String id);
	public List<Città> getAll();
	public void removeCittà(String id);
	public void updateCittà(Città c);
	public List<Città> getWhere(String field, String value);

	

}
