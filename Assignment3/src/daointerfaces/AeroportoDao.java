package daointerfaces;

import java.util.List;

import entities.Aeroporto;

public interface AeroportoDao {
	public int persistAeroporto(Aeroporto a);
	public Aeroporto findAeroporto(String AirportCode);
	public List<Aeroporto> getAll();
	public void removeAeroporto(String AirportoCode);
	public void updateAeroporto(Aeroporto a);
	public List<Aeroporto> getWhere(String field, String value);

}
