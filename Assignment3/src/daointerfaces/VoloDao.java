package daointerfaces;

import java.util.List;

import entities.Aeroporto;
import entities.Volo;

public interface VoloDao {
	public int persistVolo(Volo v);
	public Volo findVolo(String flightCode);
	public List<Volo> getAll();
	public void removeVolo(String flightCode);
	public void updateVolo(Volo v);
	public List<Volo> getWhere(String field, String value);


}
