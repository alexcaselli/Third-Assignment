package entities;

import helpers.PersistenceHelperSingleton;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Volo implements Serializable {
	
	
	
	@Id
	private String flightcode;
	
	private String aereomobile;
	
	@ManyToOne(targetEntity=Aeroporto.class) 
	@JoinColumn(name = "partenza")
	private Aeroporto partenza;
	
	@ManyToOne(targetEntity=Aeroporto.class) 
	@JoinColumn(name = "arrivo")
	private Aeroporto arrivo;
	
	@ManyToOne(targetEntity=Compagnia.class) 
	@JoinColumn(name = "compagnia")
	private Compagnia compagnia;
	
	@OneToOne(targetEntity = Volo.class, cascade = CascadeType.MERGE) 
	@PrimaryKeyJoinColumn(name="voloPrecedente_flightCode")
	private Volo voloPrecedente;
	
	public Volo(){
		super();
		
	};
	
	public Volo(String flightcode, String aereomobile, Aeroporto partenza,
			Aeroporto arrivo, Compagnia compagnia, Volo voloPrecedente) {
		
		
		this.setFlightcode(flightcode);
		this.setAereomobile(aereomobile);
		this.setPartenza(partenza);
		this.setArrivo(arrivo);
		this.setCompagnia(compagnia);
		this.setVoloPrecedente(voloPrecedente);
		
	}


	@Override
	public String toString() {
		return "Volo [flightcode=" + flightcode + ", aereomobile="
				+ aereomobile + ", voloPrecedente=" + voloPrecedente + ", Partenza=" + partenza.getAirportCode() + ", Arrivo=" + arrivo.getAirportCode() + ", Compagnia=" + compagnia.getNome()+ "]";
	}


	public String getFlightcode() {
		return flightcode;
	}


	public void setFlightcode(String flightcode) {
			this.flightcode = flightcode;

	}


	public String getAereomobile() {
		return aereomobile;
	}


	public void setAereomobile(String aereomobile) {
	
		this.aereomobile = aereomobile;
	}


	public Aeroporto getPartenza() {
		return partenza;
	}


	public void setPartenza(Aeroporto partenza) {
		this.partenza = partenza;
	}


	public Aeroporto getArrivo() {
		return arrivo;
	}


	public void setArrivo(Aeroporto arrivo) {
		this.arrivo = arrivo;
	}


	public Compagnia getCompagnia() {
		return compagnia;
	}


	public void setCompagnia(Compagnia compagnia) {
		this.compagnia = compagnia;
	}


	public Volo getVoloPrecedente() {
		return voloPrecedente;
	}


	public void setVoloPrecedente(Volo voloPrecedente) {
		this.voloPrecedente = voloPrecedente;
	}



	
	

}
