package entities;

import java.io.Serializable;

import javax.persistence.*;

import org.eclipse.persistence.annotations.CascadeOnDelete;

@Entity 
@CascadeOnDelete
public class Città implements Serializable {
	
	@Id 
	private String idCittà;
	private String nome;
	private String nazione;
	 
	public Città() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Città(String id, String nome, String nazione) {
		super();
		this.setId(id);
		this.setNome(nome);
		this.setNazione(nazione);
	}
	
	
	public String getId() {
		return idCittà;
	}
	public void setId(String id) {
		this.idCittà = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNazione() {
		return nazione;
	}
	public void setNazione(String nazione) {
		this.nazione = nazione;
	}
	
}
	
	
	
	
	
	


