package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Compagnia implements Serializable {
	
	@Id
	private String companyCode;
	
	private String nome;
	
	

	public Compagnia(String companyCode, String nome) {
		super();
		this.setCompanyCode(companyCode);
		this.setNome(nome);
	}

	public Compagnia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	


}
