package daoImplementations;

import java.util.List;

import helpers.PersistenceHelperSingleton;
import daointerfaces.AeroportoDao;
import entities.Aeroporto;
import entities.Città;

public class AeroportoDaoImpl implements AeroportoDao {
	
	PersistenceHelperSingleton phelp;
	

	public AeroportoDaoImpl() {
		phelp = PersistenceHelperSingleton.getInstance();
	}

	@Override
	public int persistAeroporto(Aeroporto a) {
		// TODO Auto-generated method stub
		int res = 0; 
		phelp.startUpdate();
		phelp.getEm().merge(a);
		phelp.commitUpdate();
		return res;
	}

	@Override
	public Aeroporto findAeroporto(String airportCode) {
		// TODO Auto-generated method stub
		return phelp.getEm().find(Aeroporto.class, airportCode);
	}
	

	@Override
	public List<Aeroporto> getAll() {
		return phelp.getEm().createQuery("SELECT e FROM Aeroporto e", Aeroporto.class).getResultList();
	}

	@Override
	public void removeAeroporto(String airportCode) {
		Aeroporto a = this.findAeroporto(airportCode);
		phelp.startUpdate();
		phelp.getEm().remove(a);
		phelp.commitUpdate();

	}

	@Override
	public void updateAeroporto(Aeroporto a) {
		phelp.startUpdate();
		phelp.getEm().merge(a);
		phelp.commitUpdate();

	}

	@Override
	public List<Aeroporto> getWhere(String field, String value) {
		return phelp.getEm()
                .createQuery("SELECT a FROM Aeroporto a WHERE a." + field + " LIKE :value", Aeroporto.class)
                .setParameter("value", value)
                .getResultList();

	}

}
