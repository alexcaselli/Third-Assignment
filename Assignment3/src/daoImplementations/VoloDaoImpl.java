package daoImplementations;

import java.util.List;

import helpers.PersistenceHelperSingleton;
import daointerfaces.VoloDao;
import entities.Aeroporto;
import entities.Volo;


public class VoloDaoImpl implements VoloDao {

PersistenceHelperSingleton phelp;
	

	public VoloDaoImpl() {
		phelp = PersistenceHelperSingleton.getInstance();
	}

	@Override
	public int persistVolo(Volo v) {
		// TODO Auto-generated method stub
		int res = 0; 
		phelp.startUpdate();
		phelp.getEm().persist(v);
		phelp.commitUpdate();
		return res;
	}

	@Override
	public Volo findVolo(String flightCode) {
		// TODO Auto-generated method stub
		return phelp.getEm().find(Volo.class, flightCode);
	}
	
	@Override
	public List<Volo> getAll() {
		return phelp.getEm().createQuery("SELECT e FROM Volo e", Volo.class).getResultList();
	}


	@Override
	public void removeVolo(String flightCode) {
		Volo v = this.findVolo(flightCode);
		phelp.startUpdate();
		phelp.getEm().remove(v);
		phelp.commitUpdate();

	}

	@Override
	public void updateVolo(Volo v) {
		phelp.startUpdate();
		phelp.getEm().merge(v);
		phelp.commitUpdate();

	}
	
	@Override
	public List<Volo> getWhere(String field, String value) {
		return phelp.getEm()
                .createQuery("SELECT v FROM Volo v WHERE v." + field + " LIKE :value", Volo.class)
                .setParameter("value", value)
                .getResultList();

	}
}
