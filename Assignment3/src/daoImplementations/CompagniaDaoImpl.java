package daoImplementations;

import java.util.List;

import helpers.PersistenceHelperSingleton;
import daointerfaces.CompagniaDao;
import entities.Aeroporto;
import entities.Compagnia;
import entities.Compagnia;

public class CompagniaDaoImpl implements CompagniaDao {

PersistenceHelperSingleton phelp;
	

	public CompagniaDaoImpl() {
		phelp = PersistenceHelperSingleton.getInstance();
	}

	@Override
	public int persistCompagnia(Compagnia c) {
		// TODO Auto-generated method stub
		int res = 0; 
		phelp.startUpdate();
		phelp.getEm().merge(c);
		phelp.commitUpdate();
		return res;
	}

	@Override
	public Compagnia findCompagnia(String companyCode) {
		// TODO Auto-generated method stub
		return phelp.getEm().find(Compagnia.class, companyCode);
	}

	@Override
	public List<Compagnia> getAll() {
		return phelp.getEm().createQuery("SELECT e FROM Compagnia e", Compagnia.class).getResultList();
	}

	@Override
	public void removeCompagnia(String companyCode) {
		Compagnia c = this.findCompagnia(companyCode);
		phelp.startUpdate();
		phelp.getEm().remove(c);
		phelp.commitUpdate();

	}

	@Override
	public void updateCompagnia(Compagnia c) {
		phelp.startUpdate();
		phelp.getEm().merge(c);
		phelp.commitUpdate();

	}
	
	@Override
	public List<Compagnia> getWhere(String field, String value) {
		return phelp.getEm()
                .createQuery("SELECT c FROM Compagnia c WHERE c." + field + " LIKE :value", Compagnia.class)
                .setParameter("value", value)
                .getResultList();

	}

}
