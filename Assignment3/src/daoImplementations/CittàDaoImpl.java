package daoImplementations;

import java.util.List;

import helpers.PersistenceHelperSingleton;
import daointerfaces.CittàDao;
import entities.Aeroporto;
import entities.Città;

public class CittàDaoImpl implements CittàDao {
	
	PersistenceHelperSingleton phelp;
	
	

	public CittàDaoImpl() {
		
		phelp = PersistenceHelperSingleton.getInstance();
		
	}

	@Override
	public int persistCittà(Città c) {
		// TODO Auto-generated method stub
		int res = 0; 
		phelp.startUpdate();
		phelp.getEm().merge(c);
		phelp.getEm().flush();
		phelp.commitUpdate();
		return res;
		
	}

	public Città findCittà(String id) {
		// TODO Auto-generated method stub
		return phelp.getEm().find(Città.class, id);
	}
	
	@Override
	public List<Città> getAll() {
		return phelp.getEm().createQuery("SELECT e FROM Città e", Città.class).getResultList();
	}




	@Override
	public void removeCittà(String id) {
		Città c = this.findCittà(id);
		phelp.startUpdate();
		phelp.getEm().remove(c);
		phelp.commitUpdate();
		
		// TODO Auto-generated method stub

	}
	
	@Override
	public void updateCittà(Città c){
		phelp.startUpdate();
		phelp.getEm().merge(c);
		phelp.commitUpdate();
	}
	@Override
	public List<Città> getWhere(String field, String value) {
		return phelp.getEm()
                .createQuery("SELECT c FROM Città c WHERE c." + field + " LIKE :value", Città.class)
                .setParameter("value", value)
                .getResultList();

	}

}
