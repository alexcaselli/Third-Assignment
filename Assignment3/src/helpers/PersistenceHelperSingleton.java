package helpers;

import java.io.Serializable;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Aeroporto;
import entities.Città;
import entities.Compagnia;
import entities.IdCittà;
import entities.Volo;

public class PersistenceHelperSingleton implements Serializable {
	
	EntityManagerFactory emf;
	EntityManager em;
	private static PersistenceHelperSingleton persistenceHelperSingleton;
	
	private PersistenceHelperSingleton() {
		
		/* Create EntityManagerFactory */
		 emf = Persistence
				.createEntityManagerFactory("Assignment3");

		/* Create EntityManager */
		 em = emf.createEntityManager();
	
	}
	
	public static PersistenceHelperSingleton getInstance(){
		if(persistenceHelperSingleton == null)
			persistenceHelperSingleton = new PersistenceHelperSingleton();
		return persistenceHelperSingleton;
	}
	
	public EntityManager getEm(){
		return em;
	}
	
	
	public void startUpdate(){
		em.getTransaction().begin();
		
	}
	public void commitUpdate(){
		em.getTransaction().commit();
		
	}
	
	

	public void close(){
		em.close();
		emf.close();
	}
	
	public void clearDatabase() {
      
        this.startUpdate();
       
        em.createNativeQuery("DELETE IGNORE FROM " + "VOLO" +" WHERE 1").executeUpdate();
        em.createNativeQuery("DELETE IGNORE FROM " + "COMPAGNIA" + " WHERE 1").executeUpdate();
        em.createNativeQuery("DELETE IGNORE FROM " + "AEROPORTO" + " WHERE 1").executeUpdate();
        em.createNativeQuery("DELETE IGNORE FROM " + "CITTÀ" + " WHERE 1").executeUpdate();
        
        this.commitUpdate();
    }


	
}
