package services;

import java.util.List;

import daoImplementations.CompagniaDaoImpl;
import entities.Aeroporto;
import entities.Compagnia;

public class CompagniaService {
	CompagniaDaoImpl cdao;

	public CompagniaService() {
	
		cdao = new CompagniaDaoImpl();

	}
	
	
	public int persist(Compagnia c) {
		int res = 0; 
		res = cdao.persistCompagnia(c);
		return res;
		
	}

	public Compagnia get(String companyCode) {
		return cdao.findCompagnia(companyCode);
	}
	
	public List<Compagnia> getAll(){
		return cdao.getAll();
	}

	public List<Compagnia> getWhere(String field, String value){
		return cdao.getWhere(field, value);
	}

	public void remove(String companyCode) {
		cdao.removeCompagnia(companyCode);
	

	}
	
	public void updateName(String companyCode, String newName){
		Compagnia c = this.get(companyCode);
		if (newName != null && newName.length() > 0 && c != null){
			c.setNome(newName);
			cdao.updateCompagnia(c);
		}
	}
	
	


}
