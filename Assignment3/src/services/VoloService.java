package services;

import java.util.List;

import daoImplementations.VoloDaoImpl;
import entities.Aeroporto;
import entities.Volo;


public class VoloService {
	VoloDaoImpl vdao;

	public VoloService() {
	
		vdao = new VoloDaoImpl();

	}
	
	
	public int persist(Volo v) {
		int res = 0; 
		res = vdao.persistVolo(v);
		return res;
		
	}

	public Volo get(String flightCode) {
		return vdao.findVolo(flightCode);
	}
	
	public List<Volo> getAll(){
		return vdao.getAll();
	}
	
	public List<Volo> getWhere(String field, String value){
		return vdao.getWhere(field, value);
	}



	public void remove(String flightCode) {
		vdao.removeVolo(flightCode);
	

	}
	
	public void updateVoloPrecendente(String flightCode, String newPreceed){
		Volo v = this.get(flightCode);
		if (newPreceed != null && newPreceed.length() > 0 && v != null){
			Volo p = this.get(newPreceed);
			v.setVoloPrecedente(p);
			vdao.updateVolo(v);
		}
	}
	public void updateAicraft(String flightCode, String newAircraft){
		Volo v = this.get(flightCode);
		if (newAircraft != null && newAircraft.length() > 0 && v != null){
			v.setAereomobile(newAircraft);
			vdao.updateVolo(v);
		}
	}
	public int updateDestinazione(String flightCode, String newAirport){
		Volo v = this.get(flightCode);
		AeroportoService as = new AeroportoService();
		Aeroporto arrivo = as.get(newAirport);
		if (arrivo != null && newAirport != null && newAirport.length() > 0 && v != null){
			
			v.setArrivo(arrivo);
			vdao.updateVolo(v);
			return 0;
		}
		
		else
			return -1;
			
	}

}
