package services;

import java.util.List;

import daoImplementations.AeroportoDaoImpl;
import daoImplementations.CittàDaoImpl;
import entities.Aeroporto;
import entities.Città;

public class AeroportoService {
	
	AeroportoDaoImpl adao;

	public AeroportoService() {
	
		adao = new AeroportoDaoImpl();

	}
	
	
	public int persist(Aeroporto a) {
		int res = 0; 
		res = adao.persistAeroporto(a);
		return res;
		
	}

	public Aeroporto get(String airportCode) {
		return adao.findAeroporto(airportCode);
	}
	
	public List<Aeroporto> getAll(){
		return adao.getAll();
	}
	
	public List<Aeroporto> getWhere(String field, String value){
		return adao.getWhere(field, value);
	}



	public void remove(String airportCode) {
		adao.removeAeroporto(airportCode);
	

	}
	
	public void updateNTerminal(String airportCode, int newNumber){
		Aeroporto a = this.get(airportCode);
		if (newNumber > 0 && a != null){
			a.setnTerminal(newNumber);
			adao.updateAeroporto(a);
		}
	}
	
	public void updateName(String airportCode, String newName){
		Aeroporto a = this.get(airportCode);
		if (newName != null && newName.length() > 0 && a != null){
			a.setNome(newName);
			adao.updateAeroporto(a);
		}
	}
	
	

}
