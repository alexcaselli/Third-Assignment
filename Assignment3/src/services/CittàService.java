package services;

import java.util.List;

import daoImplementations.CittàDaoImpl;
import entities.Aeroporto;
import entities.Città;

public class CittàService {
	
	CittàDaoImpl cdao;

	public CittàService() {
	
		cdao = new CittàDaoImpl();

	}
	
	
	public int persist(Città c) {
		int res = 0; 
		res = cdao.persistCittà(c);
		return res;
		
	}

	public Città get(String id) {
		return cdao.findCittà(id);
	}
	
	public List<Città> getAll(){
		return cdao.getAll();
	}
	
	public List<Città> getWhere(String field, String value){
		return cdao.getWhere(field, value);
	}

	public void remove(String id) {
		cdao.removeCittà(id);
	

	}
	
	public void updateName(String id, String newName){
		Città c = this.get(id);
		if (newName != null && newName.length() != 0 && c != null){
			c.setNome(newName);
			cdao.updateCittà(c);
		}
	}
	
	public void updateNazione(String id, String newNazione){
		Città c = this.get(id);
		if (newNazione!= null && newNazione.length() != 0 && c != null){
			c.setNazione(newNazione);
			cdao.updateCittà(c);
		}
	}
	
	

}
