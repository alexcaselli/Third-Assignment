package dataset;

import java.util.ArrayList;

import entities.Aeroporto;
import entities.Città;
import entities.Compagnia;
import entities.Volo;

public class Datas {
	
	//Dataset
	
		private ArrayList<Aeroporto> aeroporti;
		private ArrayList<Volo> voli;
		private ArrayList<Città> città;
		private ArrayList<Compagnia> compagnie;
		
		public Datas (){
			
			aeroporti = new ArrayList<>();
			voli = new ArrayList<>();
			città = new ArrayList<>();
			compagnie = new ArrayList<>();
			
			città.add(new Città("BGI", "Bergamo", "Italia"));
			città.add(new Città("MII", "Milano", "Italia"));
			città.add(new Città("RMI", "Roma", "Italia"));
			città.add(new Città("FRP", "Faro", "Portogallo"));
			città.add(new Città("LGS", "Lugano", "Svizzera"));
			città.add(new Città("MUG", "Monaco", "Germania"));
			città.add(new Città("LSP", "Lisbona", "Portogallo"));
			città.add(new Città("LNI", "Londra", "Inghilterra"));
			
			aeroporti.add(new Aeroporto("BGY", "Orio al Serio", 1, città.get(0)));
			aeroporti.add(new Aeroporto("MXP", "Malpensa", 2, città.get(1)));
			aeroporti.add(new Aeroporto("ROM", "Roma ARP", 1, città.get(2)));
			aeroporti.add(new Aeroporto("FRO", "Aeroporto di Faro", 1, città.get(3)));
			aeroporti.add(new Aeroporto("LGA", "Aeroporto di Lugano", 1, città.get(4)));
			aeroporti.add(new Aeroporto("MNK", "Aeroporto di Monaco", 1, città.get(5)));
			aeroporti.add(new Aeroporto("LSB", "Aeroporto di Lisbona", 2, città.get(6)));
			aeroporti.add(new Aeroporto("LNSTD", "Aeroporto di London Stansted", 1, città.get(7)));
			
			
			
			compagnie.add(new Compagnia("ESJ", "EasyJet"));
			compagnie.add(new Compagnia("ALI", "Alitalia"));
			compagnie.add(new Compagnia("RYA", "Ryanair"));
			compagnie.add(new Compagnia("NWA", "NorwayAir"));
			compagnie.add(new Compagnia("LTS", "Luftansa"));
			compagnie.add(new Compagnia("EMR", "Emirates"));
			
			voli.add(new Volo("FR146", "BOEING 373-800", aeroporti.get(1), aeroporti.get(2), compagnie.get(0), null));
			voli.add(new Volo("FR147", "BOEING 373-800", aeroporti.get(2), aeroporti.get(3), compagnie.get(0), voli.get(0)));
			voli.add(new Volo("BH234", "BOEING 616-300", aeroporti.get(0), aeroporti.get(3), compagnie.get(2), null));
			voli.add(new Volo("BH235", "BOEING 616-300", aeroporti.get(3), aeroporti.get(1), compagnie.get(2), voli.get(2)));
			voli.add(new Volo("DR456", "BOEING 716-500", aeroporti.get(4), aeroporti.get(3), compagnie.get(3), null));
			voli.add(new Volo("DR457", "BOEING 716-500", aeroporti.get(3), aeroporti.get(5), compagnie.get(3), null));
			voli.add(new Volo("AP345", "BOEING 816-508", aeroporti.get(5), aeroporti.get(6), compagnie.get(4), null));
			voli.add(new Volo("AP332", "BOEING 816-567", aeroporti.get(7), aeroporti.get(6), compagnie.get(4), null));
			voli.add(new Volo("AP333", "BOEING 816-567", aeroporti.get(7), aeroporti.get(1), compagnie.get(4), voli.get(7)));
		
		}

		public ArrayList<Aeroporto> getAeroporti() {
			return aeroporti;
		}

		public ArrayList<Volo> getVoli() {
			return voli;
		}

		public ArrayList<Città> getCittà() {
			return città;
		}

		public ArrayList<Compagnia> getCompagnie() {
			return compagnie;
		}
		
		

}
