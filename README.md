# Third Assignment Alex Caselli 807129

## Link Repository
https://gitlab.com/alexcaselli/Third-Assignment.git

## Software e framework necessari
 * Eclipse java EE
 * EclipseLink
 * MySQL 5.7
 * MySQL connector/j (https://dev.mysql.com/downloads/connector/j/8.0.html)

## Installazione
1.  Scaricare ed installare tutte le componenti elencate
2.  Clonare la repository nella cartella che verrà utilizzata come workspace di Eclipse
3.  Creare tramite MySQL un database di nome "assignment"
4.  Aprire Eclipse, selezionare il workspace selezionato precedentemente
5.  In Eclipse: File --> Import --> Existing Project into workspace --->  selezionare la directory "Third-Assignment"

## Esecuzione dei test
Le classi di test per ogni entità si trovano in test/tests/...
Per l'esecuzione:
* Aprire la classe di test desiderata
* Premere il tasto Run / Scegliere Run as.. JUNIT test


